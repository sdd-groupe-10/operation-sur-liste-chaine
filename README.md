# Operation-liste-chaine-
Petit programme permettant de faire des opérations sur une liste chaînée  en langange C.
Il permet de : 
	1. Construire cette liste chaînée d'un ou plusieurs nombres.
	2. Afficher toute la liste construite.
	3. Calculer la longueur de la liste chaînée.
	4. Ajouter un nombre dans la liste de nombres en tête de liste.
	5. Ajouter un nombre dans la liste chaînée à la position k (prévoir les cas d'insertion possibles).
	6. Rechercher un élément dans la liste.
	7. Supprimer un nombre de la liste (prévoir tous les cas possibles).
	8. Trier cette liste de nombres dans l'ordre croissant.
	9. Modifier un nombre (vérifier l'existence du nombre dans la liste).
	10. Ajouter un nombre au bon endroit dans la liste triée.
  
  

